<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_cancer extends Model
{
    protected $table = 'tipo_cancer';
    
    
    protected $fillable = [
        'nombre', 'descripcion',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];
}