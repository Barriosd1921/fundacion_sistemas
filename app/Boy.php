<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boy extends Model
{
    protected $table = 'boys';
    
    
    protected $fillable = [
        'nombres', 'apellidos', 'direccion', 'descripcion', 'id_ciudad', 'id_representante',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];
}

    
    
   
   

