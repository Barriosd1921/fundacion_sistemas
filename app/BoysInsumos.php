<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoysInsumos extends Model
{
    protected $table = 'boys_insumos';
    
    
    protected $fillable = [
        'is_active','id_boy', 'id_insumo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];
}