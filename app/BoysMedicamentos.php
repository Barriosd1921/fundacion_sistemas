<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoysMedicamentos extends Model
{
    protected $table = 'boys_medicamentos';
    
    
    protected $fillable = [
        'id_boy', 'id_medicamento','is_active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];
}