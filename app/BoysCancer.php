<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoysCancer extends Model
{
    protected $table = 'boys_cancer';
    
    
    protected $fillable = [
        'id_boy', 'id_cancer',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];
}