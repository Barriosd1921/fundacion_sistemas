<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use PDOException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    
    public function render($request, Exception $exception)
    { echo $exception;
        if($exception instanceof PDOException) 
            abort('500');
        
        if($this->isHttpException($exception)){
            switch ($exception->getStatusCode()) {
                // PAGINA NO ENCONTRADA
                case 404:
                    return response()->view('errors.404',[],404);
                break;
                // ERROR INTERNO DEL SERVIDOR
                case 500:
                    return response()->view('errors.500',[],500);
                break;
                case 403:
                    return response()->view('errors.403',[],403);
                break;
                default:
                    return redirect('/');
                break;
            }
        }
        return parent::render($request, $exception);
    }
}
