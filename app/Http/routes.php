<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'softwareController@welcome');

Route::get('/login', 'softwareController@login');

Route::post('/login', 'softwareController@inicia')->middleware('ValidarClientPI');

Route::get('/registro', 'softwareController@registro');

Route::post('/registro', 'RepresentanteController@store');

Route::post('/registrokid', 'BoyController@store')->middleware('auth');

Route::get('/lista', 'softwareController@lista');

Route::get('/listkids', 'softwareController@listkids')->middleware('auth');

Route::get('/insumodonado/{id}', 'softwareController@insumodonado')->middleware('auth');

Route::get('/medicamentodonado/{id}', 'softwareController@medicamentodonado')->middleware('auth');

Route::get('/insumoagotado/{id}', 'softwareController@insumoagotado')->middleware('auth');

Route::get('/medicamentoagotado/{id}', 'softwareController@medicamentoagotado')->middleware('auth');

Route::get('/data', 'softwareController@data')->middleware('auth');

Route::get('/exit', 'softwareController@exitUser')->middleware('auth');

Route::get('/search/{id}', 'softwareController@search');

Route::get('/medicamentos', 'softwareController@medicamentos');

Route::post('/medicamento', 'softwareController@listmedicamento');

Route::get('/insumos', 'softwareController@insumos');

Route::post('/insumo', 'softwareController@listinsumo');

Route::get('/ciudad', 'softwareController@ciudad');

Route::post('/ciudades', 'softwareController@listciudad');

Route::get('/atencion', 'softwareController@atencion');

Route::post('/atencion', 'softwareController@message');


///*
Route::get('/admin', 'softwareController@admin')->middleware('auth');

Route::post('/resetpassword', 'softwareController@resetpassword')->middleware('auth');

Route::post('/create/{type}', 'softwareController@create')->middleware('auth');
//*/

 /*
Route::get('/admin', 'softwareController@admin');

Route::post('/resetpassword', 'softwareController@resetpassword');
 */
