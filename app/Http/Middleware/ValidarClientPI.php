<?php


namespace App\Http\Middleware;
use App\User;
use Auth;
use Closure;
use Illuminate\Support\Facades\Crypt;

class ValidarClientPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {   
        if(Auth::guest()) {
            
            if(!(isset($request->email)) || !(isset($request->password)))
                abort('404');
            
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) 
                return $next($request); 
            else 
                return redirect('registro');
        } else
            return $next($request); 
            
    }
}