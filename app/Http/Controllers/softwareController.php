<?php

namespace App\Http\Controllers;

use App\Bitacora as Bitacora;
use App\Boy as Boy;
use App\Insumos as Insumos;
use App\Medicamentos as Medicamentos;
use App\Estado as Estado;
use App\BoysCancer as BoysCancer;
use App\Ciudad as Ciudad;
use App\BoysMedicamentos;
use App\BoysInsumos;
use App\Tipo_cancer as Cancer;
use App\Representante as Representante;
use Illuminate\Http\Request;
use Auth;
use Mail;
use Swift_SwiftException;

use App\Http\Requests;

class softwareController extends Controller
{

    public function create(Request $request) {


      $email = Auth::User()->email;
      $host;



      if (isset($_SERVER["HTTP_CLIENT_IP"]))
      {
      $host = $_SERVER["HTTP_CLIENT_IP"];
      }
      elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
      {
      $host = $_SERVER["HTTP_X_FORWARDED_FOR"];
      }
      elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
      {
      $host = $_SERVER["HTTP_X_FORWARDED"];
      }
      elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
      {
      $host = $_SERVER["HTTP_FORWARDED_FOR"];
      }
      elseif (isset($_SERVER["HTTP_FORWARDED"]))
      {
      $host = $_SERVER["HTTP_FORWARDED"];
      }
      else
      {
      $host = $_SERVER["REMOTE_ADDR"];
      }




      if($request->type == 1) {

        $medicamento = new Medicamentos();
        $medicamento->descripcion = $request->descripcion;
        $medicamento->nombre = $request->nombre;
        $medicamento->save();

        $bitacora = new Bitacora();
        $bitacora->email = $email;
        $bitacora->fecha = date("Y-m-d");
        $bitacora->action = 'CREATE '.'TABLE Medicamentos';
        $bitacora->host = $host;
        $bitacora->save();

        return view('/admin', ['type' => 1]);
      } 
      if($request->type == 2){

        $insumo = new Insumos();
        $insumo->descripcion = $request->descripcion;
        $insumo->nombre = $request->nombre;
        $insumo->save();

        $bitacora = new Bitacora();
        $bitacora->email = $email;
        $bitacora->fecha = date("Y-m-d");
        $bitacora->action = 'CREATE '.'TABLE Insumos';
        $bitacora->host = $host;
        $bitacora->save();

        return view('/admin', ['type' => 2]);
      }

      if($request->type == 3){

        $Tcancer = new Cancer();
        $Tcancer->descripcion = $request->descripcion;
        $Tcancer->nombre = $request->nombre;
        $Tcancer->save();

        $bitacora = new Bitacora();
        $bitacora->email = $email;
        $bitacora->fecha = date("Y-m-d");
        $bitacora->action = 'Insert in '.'TABLE Tipo_cancer';
        $bitacora->host = $host;
        $bitacora->save();

        return view('/admin', ['type' => 3]);
      }
    }

    public function resetpassword(Request $request) {

      $email = Auth::User()->email;
      $host;



      if (isset($_SERVER["HTTP_CLIENT_IP"]))
      {
      $host = $_SERVER["HTTP_CLIENT_IP"];
      }
      elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
      {
      $host = $_SERVER["HTTP_X_FORWARDED_FOR"];
      }
      elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
      {
      $host = $_SERVER["HTTP_X_FORWARDED"];
      }
      elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
      {
      $host = $_SERVER["HTTP_FORWARDED_FOR"];
      }
      elseif (isset($_SERVER["HTTP_FORWARDED"]))
      {
      $host = $_SERVER["HTTP_FORWARDED"];
      }
      else
      {
      $host = $_SERVER["REMOTE_ADDR"];
      }


      if(isset($request->email) && isset($request->password)) {
          if(Auth::User()->email == $request->email) {
               $representante = Representante::where('email', $request->email)
                                               ->update(['password' => \Hash::make($request->password)]);



                                               $bitacora = new Bitacora();
                                               $bitacora->email = $email;
                                               $bitacora->fecha = date("Y-m-d");
                                               $bitacora->action = 'UPDATE_TO  '.'TABLE Representante';
                                               $bitacora->host = $host;
                                               $bitacora->save();
          } else
          return view('welcome');

      return view('/admin', ['type' => 0]);
    }
    }

    public function admin() {
      return view('admin');
    }

    public function welcome() {
        return view('welcome');
    }

    public function login() {
        if(Auth::guest())
            return view('login');
        else
            return redirect('data');
    }

    public function listkids() {
      $medicamentos = Medicamentos::All();
      $insumos = Insumos::All();
      $estados = Estado::All();
      $ciudades = Ciudad::All();
      $cancers = Cancer::All();
      $boysCancer = BoysCancer::All();
      $boysMedicamentos = BoysMedicamentos::All();
      $boysInsumos = BoysInsumos::All();


      //$kids = Boy::All();
      $kids1 = Boy::where('id_representante', Auth::User()->id)->get();

      $kids = [];
      foreach($kids1 as $kid1)
          array_push($kids, $kid1);


      foreach($kids as $kid) {
          foreach($ciudades as $myCiudad)
              if($kid->id_ciudad == $myCiudad->id) {
                  $kid->ciudad = $myCiudad->nombre;



                  foreach($estados as $estado)
              if($myCiudad->id_estado == $estado->id)
                  $kid->estado = $estado->nombre;


              }

          $cancersFather = [];

          foreach($boysCancer as $itemCancer) {
              if($kid->id == $itemCancer->id_boy) {

                  foreach($cancers as $myCancer)
                  if($myCancer->id == $itemCancer->id_cancer) {
                      $dato = [
                      'nombre' =>$myCancer->nombre,
                      'descripcion' => $myCancer->descripcion
                  ];

                  array_push($cancersFather, $dato);
                  }

              }
          }



          $medicamentosFather = [];

          foreach($boysMedicamentos as $itemMedicamentos) {
              if($kid->id == $itemMedicamentos->id_boy) {

                  foreach($medicamentos as $medicamento)
                  if($medicamento->id == $itemMedicamentos->id_medicamento) {
                      $dato = [
                      'nombre' =>$medicamento->nombre,
                      'descripcion' => $medicamento->descripcion,
                      'is_active' => $itemMedicamentos->is_active,
                      'id'=>$itemMedicamentos->id
                      ];

                  array_push($medicamentosFather, $dato);
                  }

              }
          }


          $insumosFather = [];

          foreach($boysInsumos as $itemInsumos) {
              if($kid->id == $itemInsumos->id_boy) {
                  
                  foreach($insumos as $insumo)
                  if($insumo->id == $itemInsumos->id_insumo) {
                      $dato = [
                      'nombre' =>$insumo->nombre,
                      'descripcion' => $insumo->descripcion,
                      'is_active' =>$itemInsumos->is_active,
                      'id'=>$itemInsumos->id
                  ];

                  array_push($insumosFather, $dato);
                  }

              }
          }



          $kid->cancers = $cancersFather;
          $kid->medicamentos = $medicamentosFather;
          $kid->insumos = $insumosFather;


      }





      return view('listkids', ['kids' => $kids, 'cancer' => $cancers, 'ciudades' => $ciudades, 'insumos' => $insumos, 'medicamentos' => $medicamentos]);
    }

    public function insumodonado (Request $request){
      $boysInsumos = BoysInsumos :: find($request->id);
      $boysInsumos->is_active = 0;
      $boysInsumos->save();
      return redirect('/listkids');
    }

    public function medicamentodonado (Request $request){
      $boysMedicamentos = BoysMedicamentos :: find($request->id);
      $boysMedicamentos->is_active = 0;
      $boysMedicamentos->save();
      return redirect('/listkids');
    }

    public function insumoagotado (Request $request){
      $boysInsumos = BoysInsumos :: find($request->id);
      $boysInsumos->is_active = 1;
      $boysInsumos->save();
      return redirect('/listkids');
    }

    public function medicamentoagotado (Request $request){
      $boysMedicamentos = BoysMedicamentos :: find($request->id);
      $boysMedicamentos->is_active = 1;
      $boysMedicamentos->save();
      return redirect('/listkids');
    }

    public function lista() {



        $medicamentos = Medicamentos::All();
        $insumos = Insumos::All();
        $estados = Estado::All();
        $ciudades = Ciudad::All();
        $cancers = Cancer::All();
        $boysCancer = BoysCancer::All();
        $boysMedicamentos = BoysMedicamentos::All();
        $boysInsumos = BoysInsumos::All();


        $kids1 = Boy::All();
        //$kids1 = Boy::where('id_representante', Auth::User()->id)->get();

        $kids = [];
        foreach($kids1 as $kid1)
            array_push($kids, $kid1);


        foreach($kids as $kid) {
            foreach($ciudades as $myCiudad)
                if($kid->id_ciudad == $myCiudad->id) {
                    $kid->ciudad = $myCiudad->nombre;



                    foreach($estados as $estado)
                if($myCiudad->id_estado == $estado->id)
                    $kid->estado = $estado->nombre;


                }

            $cancersFather = [];

            foreach($boysCancer as $itemCancer) {
                if($kid->id == $itemCancer->id_boy) {

                    foreach($cancers as $myCancer)
                    if($myCancer->id == $itemCancer->id_cancer) {
                        $dato = [
                        'nombre' =>$myCancer->nombre,
                        'descripcion' => $myCancer->descripcion
                    ];

                    array_push($cancersFather, $dato);
                    }

                }
            }



            $medicamentosFather = [];

            foreach($boysMedicamentos as $itemMedicamentos) {
                if($kid->id == $itemMedicamentos->id_boy) {

                    foreach($medicamentos as $medicamento)
                    if($medicamento->id == $itemMedicamentos->id_medicamento) {
                        $dato = [
                        'nombre' =>$medicamento->nombre,
                        'descripcion' => $medicamento->descripcion,
                        'is_active' => $itemMedicamentos->is_active
                    ];

                    array_push($medicamentosFather, $dato);
                    }

                }
            }


            $insumosFather = [];

            foreach($boysInsumos as $itemInsumos) {
                if($kid->id == $itemInsumos->id_boy) {

                    foreach($insumos as $insumo)
                    if($insumo->id == $itemInsumos->id_insumo) {
                        $dato = [
                        'nombre' =>$insumo->nombre,
                        'descripcion' => $insumo->descripcion,
                        'is_active' => $itemInsumos->is_active
                    ];

                    array_push($insumosFather, $dato);
                    }

                }
            }



            $kid->cancers = $cancersFather;
            $kid->medicamentos = $medicamentosFather;
            $kid->insumos = $insumosFather;


        }

        return view('lista', ['kids' => $kids]);
    }

    public function registro() {
        if(Auth::guest())
            return view('registro');
        else {
            if(Auth::User()->tipo != 0)
              return redirect('data');
            else
              return redirect('/admin');
        }
    }

    public function inicia(Request $request) {
      if(Auth::User()->tipo != 0)
        return redirect('data');
      else
        return redirect('/admin');
    }

    public function data(Request $request) {

      if(Auth::User()->tipo != 0) {
        $cancers = Cancer::All();
        $ciudades = Ciudad::All();
        $medicamentos = Medicamentos::All();
        $insumos = Insumos::All();

        return view('data', ['cancer' => $cancers, 'ciudades' => $ciudades, 'insumos' => $insumos, 'medicamentos' => $medicamentos]);
      } else
        return redirect('/admin');

    }





    public function exitUser(Request $request) {
        Auth::logOut();
        return redirect('/');
    }


    public function search(Request $request) {
        switch($request->id) {
            case 1:
                return redirect('/medicamentos');
                break;
            case 2:
                return redirect('/insumos');
                break;
            default:
                return redirect('/ciudad');
                break;
        }
    }


    public function ciudad() {
        $ciudades = Ciudad::All();
        return view('ciudad', ['ciudades' => $ciudades]);
    }







    public function listciudad(Request $request) {
       $medicamentos = Medicamentos::All();
        $insumos = Insumos::All();
        $estados = Estado::All();
        $ciudades = Ciudad::All();
        $cancers = Cancer::All();
        $boysCancer = BoysCancer::All();
        $boysMedicamentos = BoysMedicamentos::All();
        $boysInsumos = BoysInsumos::All();

        $kids1 = Boy::where('id_ciudad', $request->id_ciudad)->get();


         $kids = [];
        foreach($kids1 as $kid1)
            array_push($kids, $kid1);


        foreach($kids as $kid) {
            foreach($ciudades as $myCiudad)
                if($kid->id_ciudad == $myCiudad->id) {
                    $kid->ciudad = $myCiudad->nombre;



                    foreach($estados as $estado)
                if($myCiudad->id_estado == $estado->id)
                    $kid->estado = $estado->nombre;


                }

            $cancersFather = [];

            foreach($boysCancer as $itemCancer) {
                if($kid->id == $itemCancer->id_boy) {

                    foreach($cancers as $myCancer)
                    if($myCancer->id == $itemCancer->id_cancer) {
                        $dato = [
                        'nombre' =>$myCancer->nombre,
                        'descripcion' => $myCancer->descripcion
                    ];

                    array_push($cancersFather, $dato);
                    }

                }
            }



            $medicamentosFather = [];

            foreach($boysMedicamentos as $itemMedicamentos) {
                if($kid->id == $itemMedicamentos->id_boy) {

                    foreach($medicamentos as $medicamento)
                    if($medicamento->id == $itemMedicamentos->id_medicamento) {
                        $dato = [
                        'nombre' =>$medicamento->nombre,
                        'descripcion' => $medicamento->descripcion,
                        'is_active'=>$itemMedicamentos->is_active
                    ];

                    array_push($medicamentosFather, $dato);
                    }

                }
            }


            $insumosFather = [];

            foreach($boysInsumos as $itemInsumos) {
                if($kid->id == $itemInsumos->id_boy) {

                    foreach($insumos as $insumo)
                    if($insumo->id == $itemInsumos->id_insumo) {
                        $dato = [
                        'nombre' =>$insumo->nombre,
                        'descripcion' => $insumo->descripcion,
                        'is_active' => $itemInsumos->is_active
                    ];

                    array_push($insumosFather, $dato);
                    }

                }
            }



            $kid->cancers = $cancersFather;
            $kid->medicamentos = $medicamentosFather;
            $kid->insumos = $insumosFather;


        }

        return view('listciudad', ['kids' => $kids]);
    }


    public function insumos() {
        $insumos = Insumos::All();
        return view('insumos', ['insumos' => $insumos]);
    }


    public function listinsumo(Request $request) {
      $medicamentos = Medicamentos::All();
        $insumos = Insumos::All();
        $estados = Estado::All();
        $ciudades = Ciudad::All();
        $cancers = Cancer::All();
        $boysCancer = BoysCancer::All();
        $boysMedicamentos = BoysMedicamentos::All();
        $boysInsumos = BoysInsumos::All();



        $dataInsumos = BoysInsumos::where('id_insumo', $request->id_insumo)->get();



        $kids = [];
        foreach($dataInsumos as $dataInsumo)
            array_push($kids, Boy::find($dataInsumo->id_boy));






        //$kids1 = Boy::where('id_ciudad', $request->id_ciudad)->get();


        //$kids = [];
      //  foreach($kids1 as $kid1)
         //   array_push($kids, $kid1);


        foreach($kids as $kid) {
            foreach($ciudades as $myCiudad)
                if($kid->id_ciudad == $myCiudad->id) {
                    $kid->ciudad = $myCiudad->nombre;



                    foreach($estados as $estado)
                if($myCiudad->id_estado == $estado->id)
                    $kid->estado = $estado->nombre;


                }

            $cancersFather = [];

            foreach($boysCancer as $itemCancer) {
                if($kid->id == $itemCancer->id_boy) {

                    foreach($cancers as $myCancer)
                    if($myCancer->id == $itemCancer->id_cancer) {
                        $dato = [
                        'nombre' =>$myCancer->nombre,
                        'descripcion' => $myCancer->descripcion
                    ];

                    array_push($cancersFather, $dato);
                    }

                }
            }



            $medicamentosFather = [];

            foreach($boysMedicamentos as $itemMedicamentos) {
                if($kid->id == $itemMedicamentos->id_boy) {

                    foreach($medicamentos as $medicamento)
                    if($medicamento->id == $itemMedicamentos->id_medicamento) {
                        $dato = [
                        'nombre' =>$medicamento->nombre,
                        'descripcion' => $medicamento->descripcion,
                        'is_active' => $itemMedicamentos->is_active
                    ];

                    array_push($medicamentosFather, $dato);
                    }

                }
            }


            $insumosFather = [];

            foreach($boysInsumos as $itemInsumos) {
                if($kid->id == $itemInsumos->id_boy) {

                    foreach($insumos as $insumo)
                    if($insumo->id == $itemInsumos->id_insumo) {
                        $dato = [
                        'nombre' =>$insumo->nombre,
                        'descripcion' => $insumo->descripcion,
                        'is_active' => $itemInsumos->is_active
                    ];

                    array_push($insumosFather, $dato);
                    }

                }
            }



            $kid->cancers = $cancersFather;
            $kid->medicamentos = $medicamentosFather;
            $kid->insumos = $insumosFather;


        }

        return view('listinsumos', ['kids' => $kids]);
    }


    public function atencion() {
        return view('atencion');
    }


    public function medicamentos() {
        $medicamentos = Medicamentos::All();
        return view('medicamentos', ['medicamentos' => $medicamentos]);
    }



    public function listmedicamento(Request $request) {
        $medicamentos = Medicamentos::All();
        $insumos = Insumos::All();
        $estados = Estado::All();
        $ciudades = Ciudad::All();
        $cancers = Cancer::All();
        $boysCancer = BoysCancer::All();
        $boysMedicamentos = BoysMedicamentos::All();
        $boysInsumos = BoysInsumos::All();



        $dataMedicamentos = BoysMedicamentos::where('id_medicamento', $request->id_medicamento)->get();



        $kids = [];
        foreach($dataMedicamentos as $dataMedicamento)
            array_push($kids, Boy::find($dataMedicamento->id_boy));






        //$kids1 = Boy::where('id_ciudad', $request->id_ciudad)->get();


        //$kids = [];
      //  foreach($kids1 as $kid1)
         //   array_push($kids, $kid1);


        foreach($kids as $kid) {
            foreach($ciudades as $myCiudad)
                if($kid->id_ciudad == $myCiudad->id) {
                    $kid->ciudad = $myCiudad->nombre;



                    foreach($estados as $estado)
                if($myCiudad->id_estado == $estado->id)
                    $kid->estado = $estado->nombre;


                }

            $cancersFather = [];

            foreach($boysCancer as $itemCancer) {
                if($kid->id == $itemCancer->id_boy) {

                    foreach($cancers as $myCancer)
                    if($myCancer->id == $itemCancer->id_cancer) {
                        $dato = [
                        'nombre' =>$myCancer->nombre,
                        'descripcion' => $myCancer->descripcion
                    ];

                    array_push($cancersFather, $dato);
                    }

                }
            }



            $medicamentosFather = [];

            foreach($boysMedicamentos as $itemMedicamentos) {
                if($kid->id == $itemMedicamentos->id_boy) {

                    foreach($medicamentos as $medicamento)
                    if($medicamento->id == $itemMedicamentos->id_medicamento) {
                        $dato = [
                        'nombre' =>$medicamento->nombre,
                        'descripcion' => $medicamento->descripcion,
                        'is_active' => $itemMedicamentos->is_active
                    ];

                    array_push($medicamentosFather, $dato);
                    }

                }
            }


            $insumosFather = [];

            foreach($boysInsumos as $itemInsumos) {
                if($kid->id == $itemInsumos->id_boy) {

                    foreach($insumos as $insumo)
                    if($insumo->id == $itemInsumos->id_insumo) {
                        $dato = [
                        'nombre' =>$insumo->nombre,
                        'descripcion' => $insumo->descripcion,
                        'is_active' => $itemInsumos->is_active
                    ];

                    array_push($insumosFather, $dato);
                    }

                }
            }



            $kid->cancers = $cancersFather;
            $kid->medicamentos = $medicamentosFather;
            $kid->insumos = $insumosFather;


        }

        return view('listmedicamentos', ['kids' => $kids]);
    }



    public function message(Request $request) {

      Mail::send('email.myemail', ['asunto'=>$request->asunto,'email'=>$request->email,'mensaje'=>$request->messege], function ($message) use ($user){

      $message->subject('MENSAJE FUNDACION CANCER');

      $message->to('fundacioncancersistemas@gmail.com');

      });
      return view('sendmessage');
    }

}
