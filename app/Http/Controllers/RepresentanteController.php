<?php

namespace App\Http\Controllers;
use App\Representante;
use App\Bitacora;
use Illuminate\Http\Request;

use App\Http\Requests;

class RepresentanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // 'nombres', 'apellidos', 'cedula', 'telefono', 'email', 'password', 'email', 'password',

    public function store(Request $request)
    {
        $usuario = new Representante;

        $usuario->nombres = $request->nombres;
        $usuario->apellidos = $request->apellidos;
        $usuario->cedula = $request->cedula;
        $usuario->telefono = $request->telefono;
        $usuario->tipo = 1;


        $usuario->email = $request->email;
        $usuario->password = \Hash::make($request->password);

        $usuario->save();



        $email = $request->email;
        $host;



        if (isset($_SERVER["HTTP_CLIENT_IP"]))
        {
        $host = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
        $host = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
        {
        $host = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
        {
        $host = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"]))
        {
        $host = $_SERVER["HTTP_FORWARDED"];
        }
        else
        {
        $host = $_SERVER["REMOTE_ADDR"];
        }

        $bitacora = new Bitacora();
        $bitacora->email = $email;
        $bitacora->fecha = date("Y-m-d");
        $bitacora->action = 'CREATE '.'TABLE Representantes';
        $bitacora->host = $host;
        $bitacora->save();






        return redirect('data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
