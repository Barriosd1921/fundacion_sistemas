<?php

namespace App\Http\Controllers;
use App\Boy;
use App\BoysCancer;
use App\BoysMedicamentos;
use App\BoysInsumos;
use App\Bitacora;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;

class BoyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // 'nombres', 'apellidos', 'cedula', 'telefono', 'email', 'password', 'email', 'password',

    public function store(Request $request)
    {
$email = Auth::User()->email;
$host;



if (isset($_SERVER["HTTP_CLIENT_IP"]))
{
$host = $_SERVER["HTTP_CLIENT_IP"];
}
elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
{
$host = $_SERVER["HTTP_X_FORWARDED_FOR"];
}
elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
{
$host = $_SERVER["HTTP_X_FORWARDED"];
}
elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
{
$host = $_SERVER["HTTP_FORWARDED_FOR"];
}
elseif (isset($_SERVER["HTTP_FORWARDED"]))
{
$host = $_SERVER["HTTP_FORWARDED"];
}
else
{
$host = $_SERVER["REMOTE_ADDR"];
}


        if(count($request->cancer) > 0) {
        $usuario = new Boy;

        $usuario->nombres = $request->nombres;
        $usuario->apellidos = $request->apellidos;
        $usuario->direccion = $request->direccion;
        $usuario->descripcion = $request->descripcion;

        $usuario->genero = $request->genero;

        $usuario->id_ciudad = $request->id_ciudad;








        $usuario->id_representante = Auth::User()->id;




        $usuario->save();

        $bitacora = new Bitacora();
        $bitacora->email = $email;
        $bitacora->fecha = date("Y-m-d");
        $bitacora->action = 'CREATE '.'TABLE Boys';
        $bitacora->host = $host;
        $bitacora->save();

            /*
           $usuario = Boy::where('nombres', $request->nombres)->
               where('apellidos', $request->apellidos)->
               where('direccion', $request->direccion)->
               where('descripcion', $request->descripcion)->
               where('genero', $request->genero)->
               where('id_ciudad', $request->id_ciudad)->get();

            if(isset($usuario))
                echo 'esta';
            */
            foreach($request->cancer as $myCancer) {
            $cancerSave = new BoysCancer();
            $cancerSave->id_boy = $usuario->id;
            $cancerSave->id_cancer = $myCancer;
            $cancerSave->save();


            $bitacora = new Bitacora();
            $bitacora->email = $email;
            $bitacora->fecha = date("Y-m-d");
            $bitacora->action = 'CREATE '.'TABLE BoysCancer';
            $bitacora->host = $host;
            $bitacora->save();

        }



            if(isset($request->medicamentos))
            foreach($request->medicamentos as $medicamentos) {
            $cancerSave = new BoysMedicamentos();
            $cancerSave->id_boy = $usuario->id;
            $cancerSave->id_medicamento = $medicamentos;
            $cancerSave->save();

            $bitacora = new Bitacora();
            $bitacora->email = $email;
            $bitacora->fecha = date("Y-m-d");
            $bitacora->action = 'CREATE '.'TABLE BoysMedicamentos';
            $bitacora->host = $host;
            $bitacora->save();
        }

            if(isset($request->insumos))
            foreach($request->insumos as $insumos) {
            $cancerSave = new BoysInsumos();
            $cancerSave->id_boy = $usuario->id;
            $cancerSave->id_insumo = $insumos;
            $cancerSave->save();

            $bitacora = new Bitacora();
            $bitacora->email = $email;
            $bitacora->fecha = date("Y-m-d");
            $bitacora->action = 'CREATE '.'TABLE BoysInsumos';
            $bitacora->host = $host;
            $bitacora->save();
        }






             }
        return view('datasend');
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
