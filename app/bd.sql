CREATE TABLE bitacora (
    id int auto_increment PRIMARY KEY,
    email varchar(64) NOT NULL,
    host varchar(64) NOT NULL,
    action varchar(64) NOT NULL,
    fecha timestamp,
    remember_token varchar(100) DEFAULT NULL,
    created_at timestamp,
    updated_at timestamp
);

CREATE TABLE representantes (
    id int auto_increment PRIMARY KEY,
    tipo int NOT NULL,
    email varchar(64) NOT NULL,
    nombres varchar(64) NOT NULL,
    apellidos varchar(64) NOT NULL,
    cedula varchar(64) NOT NULL,
    telefono varchar(64) NOT NULL,
    password varchar(100) NOT NULL,
    remember_token varchar(100) DEFAULT NULL,
    created_at timestamp,
    updated_at timestamp
);

CREATE TABLE boys (
    id int auto_increment PRIMARY KEY,
    nombres varchar(64) NOT NULL,
    apellidos varchar(64) NOT NULL,
    direccion varchar(100) NOT NULL,
    descripcion varchar(64) NOT NULL,
    genero char NOT NULL,
    id_ciudad int NOT NULL,
    id_representante int NOT NULL,
    remember_token varchar(100) DEFAULT NULL,
    created_at timestamp,
    updated_at timestamp
);

CREATE TABLE estados (
    id int auto_increment PRIMARY KEY,
    nombre varchar(64) NOT NULL,
    remember_token varchar(100) DEFAULT NULL,
    created_at timestamp,
    updated_at timestamp
);

CREATE TABLE medicamentos (
    id int auto_increment PRIMARY KEY,
    nombre varchar(64) NOT NULL,
    descripcion varchar(64) NOT NULL,
    remember_token varchar(100) DEFAULT NULL,
    created_at timestamp,
    updated_at timestamp
);

CREATE TABLE insumos (
    id int auto_increment PRIMARY KEY,
    nombre varchar(64) NOT NULL,
    descripcion varchar(64) NOT NULL,
    remember_token varchar(100) DEFAULT NULL,
    created_at timestamp,
    updated_at timestamp
);

CREATE TABLE ciudades (
    id int auto_increment PRIMARY KEY,
    nombre varchar(64) NOT NULL,
    id_estado int NOT NULL,
    remember_token varchar(100) DEFAULT NULL,
    created_at timestamp,
    updated_at timestamp
);


CREATE TABLE boys_cancer (
    id int auto_increment PRIMARY KEY,
    id_boy int NOT NULL,
    id_cancer int NOT NULL,
    remember_token varchar(100) DEFAULT NULL,
    created_at timestamp,
    updated_at timestamp
);

CREATE TABLE boys_medicamentos (
    id int auto_increment PRIMARY KEY,
    id_boy int NOT NULL,
    is_active int DEFAULT 1,
    id_medicamento int NOT NULL,
    remember_token varchar(100) DEFAULT NULL,
    created_at timestamp,
    updated_at timestamp
);

CREATE TABLE boys_insumos (
    id int auto_increment PRIMARY KEY,
    is_active int DEFAULT 1,
    id_boy int NOT NULL,
    id_insumo int NOT NULL,
    remember_token varchar(100) DEFAULT NULL,
    created_at timestamp,
    updated_at timestamp
);


CREATE TABLE tipo_cancer (
    id int auto_increment PRIMARY KEY,
    nombre varchar(100) NOT NULL,
    descripcion varchar(300) NOT NULL,
    remember_token varchar(100) DEFAULT NULL,
    created_at timestamp,
    updated_at timestamp
);


/* ALTERACIONES A LAS TABLAS */
ALTER TABLE ciudades ADD FOREIGN KEY (id_estado) REFERENCES estados (id);
ALTER TABLE boys ADD FOREIGN KEY (id_ciudad) REFERENCES ciudades (id);
ALTER TABLE boys ADD FOREIGN KEY (id_representante) REFERENCES representantes (id);

ALTER TABLE boys_cancer ADD FOREIGN KEY (id_boy) REFERENCES boys (id);
ALTER TABLE boys_cancer ADD FOREIGN KEY (id_cancer) REFERENCES tipo_cancer (id);

ALTER TABLE boys_medicamentos ADD FOREIGN KEY (id_medicamento) REFERENCES medicamentos (id);
ALTER TABLE boys_medicamentos ADD FOREIGN KEY (id_boy) REFERENCES boys (id);

ALTER TABLE boys_insumos ADD FOREIGN KEY (id_insumo) REFERENCES insumos (id);
ALTER TABLE boys_insumos ADD FOREIGN KEY (id_boy) REFERENCES boys (id);


INSERT INTO estados (nombre) values ('Táchira');
INSERT INTO ciudades (nombre, id_estado) values ('San Cristobal', 1);
INSERT INTO ciudades (nombre, id_estado) values ('Rubio', 1);
INSERT INTO tipo_cancer (nombre, descripcion) values ('Cáncer escamoso metastásico de cuello con tumor primario oculto',
                                                      'es un cancer que require tratamiento rápido, avanza en un tiempo mas corto');
INSERT INTO tipo_cancer (nombre, descripcion) values ('Carcinoma de corteza suprarrenal', 'es un cancer que afecta a la corteza suprarrenal');

INSERT INTO medicamentos (nombre, descripcion) values ('Teragrip', 'pastilla para la gripe');
INSERT INTO medicamentos (nombre, descripcion) values ('sulbatamol', 'oxigenación');

INSERT INTO insumos (nombre, descripcion) values ('silla de rueda', 'silla de ultima genercion');
INSERT INTO insumos (nombre, descripcion) values ('baston', 'basto ortopedico');

INSERT INTO representantes (tipo, email, nombres, apellidos, cedula, telefono, password) values (0, 'master@masterfunda.com', 'master', 'master', 'master', 'master', '123456');
