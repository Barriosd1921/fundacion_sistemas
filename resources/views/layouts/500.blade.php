<div class="container level-padding">
    <div class="row">
        <div class="col a12 center-align">
            <a tabindex="1" href="{{url('')}}">
                <img alt="joalcapa logo" src="{{ asset('assets/joalcplogo.png') }}"/><br/>
            </a>
        </div>
    </div>
</div>
<div class="center-align">
    <div class="container row">
        <div class="row container">
            <div class="col a12">
                <img alt="error 500" style="width:100%; max-width:600px;" src="{{ asset('assets/500.png') }}"/>
                <div class="container black-text center-align title-text">
                    <b>error 500!</b>
                </div>
                <div class="container level-padding-bottom">
                    <p class="grey-text">
                        El servidor parece presentar problemas internos<br/>
                        porfavor intentalo más tarde
                    </p>
                </div>
            </div>
        </div>  
    </div>
</div>