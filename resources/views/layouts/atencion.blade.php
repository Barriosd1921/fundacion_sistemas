<div class="level-padding">
    <div class="container">
        <div class="row">
            <div class="col a12">
                <a tabindex="1" href="{{url('/')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large red white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            volver
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="container min-padding">
    <div class="row">
        <div class="col a12">
            <form method="POST" action="{{url('/atencion')}}">
                {{ csrf_field() }}
            <div class="black-text" style="width:100%; max-width:800px; margin:0 auto;">
            <div class="row">
            
            <div class="col a12 level-padding title-text blue-text">
                envia su mensaje con todos sus datos, pronto atenderemos a su llamado
            </div>

            <div class="col a6 min-padding">
                <input style="border-radius: 5px; border: 1px dashed blue;" type="email" name="email" placeholder="Email" required>
            </div>
            <div class="col a6 min-padding">
                <input style="border-radius: 5px; border: 1px dashed blue;" type="text" name="asunto" placeholder="Asunto" required>
            </div>

            
            <div class="col a12 min-padding">
                <textarea style="border-radius: 5px; border: 1px solid blue; " name="message" placeholder="escriba su mensaje" required></textarea>
            </div>
                
                
                
              <div class="col a12 min-padding">
                <input tabindex="8" class="cursor" type="submit" value="Send"/>
            </div>  
                
           
           
            
            </div>
            
            
          </div>            
    </form>
        </div>
    </div>
</div>