<div class="level-padding">
    <div class="container">
        <div class="row">
            <div class="col a4 t12 level-padding">
                <a tabindex="1" href="{{url('/')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large blue white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            home
                        </div>
                    </div>
                </a>
            </div>
            <div class="col a4 t12 level-padding">
                <a tabindex="2" href="{{url('/data')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large blue white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            registro
                        </div>
                    </div>
                </a>
            </div>
            <div class="col a4 t12 level-padding">
                <a tabindex="3" href="{{url('exit')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large red white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            exit
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>


@if(count($kids) == 0)
<div class="black-text subtitle-text severius-padding">
    Aun no has registrado niños
</div>
@else

<div class="container severius-padding">
    <div class="col a12 level-padding title-text blue-text">
                Niños registrados
            </div>
@foreach ($kids as $kid)
<div class="min-padding black-text">
    <div class="row left-align white z-depth-2" style="background-color:rgba(0,180,255,.5); border-radius:25px; margin-top:15px; padding-top:10px; padding-bottom:10px;">
        <div class="col a4 m6 t12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">

            Nombres: {{ $kid->nombres }} {{ $kid->apellidos }}

        </div>
        <div class="col a4 m6 t12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">

            Direccion: {{ $kid->direccion }}

        </div>
        <div class="col a4 m6 t12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">

            Estado: {{ $kid->estado }}

        </div>
        <div class="col a4 m6 t12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">

            Ciudad: {{ $kid->ciudad }}

        </div>

        <div class="col a4 m6 t12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">

            Situación: {{ $kid->descripcion }}

    </div>
     <br><br>
        <div class="col a12">
            <div class="col a12" style="border-bottom: 1px solid black; padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
            <b>LISTA DE CANCER QUE PADECE:</b>
            </div>
           
            @foreach($kid->cancers as $myCancer)
            
            <div class="min-padding">
                <div class="col a12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                Nombre: {{ $myCancer['nombre'] }}

                </div>
                <div class="col a12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                Descripcion: {{ $myCancer['descripcion'] }}
                </div>
            </div>
            @endforeach
        </div>
        
        <!-- INICIA LISTA DE INSUMOS QUE NECESITA-->

        @if(count($kid->insumos) > 0)
        <div class="col a12">
            <br>
            <div class="col a12" style="border-bottom: 1px solid black; padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                <b>LISTA DE INSUMOS QUE NECESITA:</b>
            </div>
           
           @foreach($kid->insumos as $myCancer)
            @if($myCancer['is_active']==1)
            <div class="min-padding">
                <div class="col a12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                    Nombre: {{ $myCancer['nombre'] }}
                    <a href='{{url("insumodonado/".$myCancer["id"])}}'>
                    <button class="animated infinite pulse btn-large-container center-align btn2">DONADO</button>
                    </a>
                </div>
                <div class="col a12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                Descripcion: {{ $myCancer['descripcion'] }}
                </div>
            </div>
            <br/><br/>
            @endif
            @endforeach 
        </div>
        @endif

        <!-- INICIO DE LISTA INSUMOS DONADOS -->
        @if(count($kid->insumos) > 0 )
        <div class="col a12">
            <br>
            <div class="col a12" style="border-bottom: 1px solid black; padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                <b>LISTA DE INSUMOS QUE DONADO:</b>
            </div>
           
           @foreach($kid->insumos as $myCancer)
            @if($myCancer['is_active']==0)
            <div class="min-padding">
                <div class="col a12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                    Nombre: {{ $myCancer['nombre'] }}
                    <a href='{{url("insumoagotado/".$myCancer["id"])}}'>
                    <button class="animated infinite pulse btn-large-container center-align btn1">AGOTADO</button>
                    </a>
                </div>
                <div class="col a12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                Descripcion: {{ $myCancer['descripcion'] }}
                </div>
            </div>
            <br/><br/>
            @endif
            @endforeach
        </div>
        @endif

        <!-- INICIA LISTA DE MEDICAMENTOS QUE NECESITA-->

        @if(count($kid->medicamentos) > 0)
        <div class="col a12">
            <br>
            <div class="col a12" style="border-bottom: 1px solid black; padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                <b>LISTA DE MEDICAMENTOS QUE NECESITA: </b>
            </div>
           
            @foreach($kid->medicamentos as $myCancer)
            @if($myCancer['is_active'] == 1)
            <div class="min-padding">
                <div class="col a12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                Nombre: {{ $myCancer['nombre'] }}
                <a href='{{url("medicamentodonado/".$myCancer["id"])}}'>
                <button class="animated infinite pulse btn-large-container center-align btn2">DONADO</button>
                </a>
                </div>
                <div class="col a12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                Descripcion: {{ $myCancer['descripcion'] }}
                </div>
            </div>
            @endif
            @endforeach        
            
        </div>
        @endif

        <!-- INICIA LISTA DE MEDICAMENTOS DONADOS-->

        @if(count($kid->medicamentos) > 0)
        <div class="col a12">
            <br>
            <div class="col a12" style="border-bottom: 1px solid black; padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                <b>LISTA DE MEDICAMENTOS QUE DONADOS: </b>
            </div>
            <br><br>
            @foreach($kid->medicamentos as $myCancer)
            @if($myCancer['is_active'] == 0)
            <div class="min-padding">
                <div class="col a12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                Nombre: {{ $myCancer['nombre'] }}
                <a href='{{url("medicamentoagotado/".$myCancer["id"])}}'>
                <button class="animated infinite pulse btn-large-container center-align btn1">AGOTADO</button>
                </a>
                </div>
                <div class="col a12" style="padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;">
                Descripcion: {{ $myCancer['descripcion'] }}
                </div>
            </div>
            @endif
            @endforeach        
            
        </div>
        @endif


</div>
@endforeach
</div>

@endif
