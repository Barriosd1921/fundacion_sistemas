<div class="level-padding">
    <div class="container">
        <div class="row">
            <div class="col a12">
                <a tabindex="1" href="{{url('/')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large blue white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            home
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="container level-padding">
    <div class="row">
        <div class="col a12">
            <form method="POST" action="{{url('/registro')}}">
                {{ csrf_field() }}
        <div class="black-text row">
            <div class="col a12 level-padding title-text blue-text">
                Registro de representantes
            </div>
            <p>
                por favor ingresé cuidadosamente todos los datos, al momento de registrate como representante, con esta cuenta podrás
                ingresar a los niños, para que así puedan aparecer en la lista de la fundación llenando manos vacías
            </p>
            <div class="min-padding col a6 t12">
                <input tabindex="2" type="text" placeholder="nombres" name="nombres" required />
            </div>
            <div class="min-padding col a6 t12">
                <input tabindex="3" type="text" placeholder="apellidos" name="apellidos" required/>
            </div>
            <div class="min-padding col a6 t12">
                <input tabindex="4" type="text" placeholder="cedula" name="cedula" required/>
            </div>
            <div class="min-padding col a6 t12">
                <input tabindex="5" type="text" placeholder="telefono" name="telefono" required/>
            </div>
            <div class="min-padding col a6 t12">
                <input tabindex="6" type="email" placeholder="email" name="email" required/>
            </div>
            <div class="min-padding col a6 t12">
                <input tabindex="7" type="password" placeholder="password" name="password" required/>
            </div>
            <div class="level-padding col a12">
                <input tabindex="8" class="cursor" type="submit" value="login"/>
            </div>
        </div>    
    </form>
        </div>
    </div>
</div>