<div class="container level-padding">
    <div class="row">
        <div class="col a12 center-align">
            <a tabindex="1" href="{{url('')}}">
                <img alt="joalcapa logo" src="{{ asset('assets/joalcplogo.png') }}"/><br/>
            </a>
        </div>
    </div>
</div>
<div class="center-align">
    <div class="container row">
        <div class="row container">
            <div class="col a12">
                <img alt="error 403" style="width:100%; max-width:600px;" src="{{ asset('assets/403.png') }}"/>
                <div class="container black-text center-align title-text">
                    <b>error 403!</b>
                </div>
                <div class="container level-padding-bottom">
                    <p class="grey-text">
                        <b class="black-text">Ooops!</b> &nbsp&nbsp Lo siento, no tienes permiso para realizar esta acción, ponte en contacto conmigo lo más
                        antes posible
                    </p>
                </div>
            </div>
        </div>  
    </div>
</div>