<div class="level-padding">
    <div class="container">
        <div class="row">
            <div class="col a4 t12 level-padding">
                <a tabindex="1" href="{{url('/')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large blue white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            home
                        </div>
                    </div>
                </a>
            </div>
            <div class="col a4 t12 level-padding">
                <a tabindex="2" href="{{url('/listkids')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large blue white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            lista
                        </div>
                    </div>
                </a>
            </div>
            <div class="col a4 t12 level-padding">
                <a tabindex="3" href="{{url('exit')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large red white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            exit
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>





<div class="container min-padding">
    <div class="row">
        <div class="col a12">
            <form method="POST" action="{{url('/registrokid')}}">
                {{ csrf_field() }}
        <div class="black-text" style="width:100%; max-width:800px; margin:0 auto;">


            <div class="row">

            <div class="col a12 level-padding title-text blue-text">
                Registro del niño
            </div>
            <div class="col a6 t12 min-padding">
                <input tabindex="2" type="text" placeholder="nombres" name="nombres" required />
            </div>
            <div class="col a6 t12 min-padding">
                <input tabindex="3" type="text" placeholder="apellidos" name="apellidos" required/>
            </div>
            <div class="col a6 t12 min-padding">
                <input tabindex="4" type="text" placeholder="direccion" name="direccion" required/>
            </div>
            <div class="col a6 t12 min-padding">
                <input tabindex="5" type="text" placeholder="situacion" name="descripcion" required/>
            </div>
            <div class="col a6 t12 min-padding">
                <select name="genero"> <!--Supplement an id here instead of using 'name'-->
                    <option value="M">masculino</option>
                    <option value="F" selected>femenino</option>
                </select>
            </div>
            <div class="col a6 t12 min-padding">
                <select name="id_ciudad"> <!--Supplement an id here instead of using 'name'-->
                  <option >Ciudad</option>
                    @foreach ($ciudades as $ciudad)
                    <option value="{{ $ciudad->id }}">{{ $ciudad->nombre }}</option>
                    @endforeach
                </select>
            </div>
                <div class="col a12  min-padding center-align">
                Seleccioné los tipos de cancer que padece el niño:<br/>
                    </div>
                <div class="col a12  min-padding center-align">
                  <select name="cancer[]" multiple> <option >tipo de cancer</option>
                @foreach ($cancer as $cance)


  <option value="{{ $cance->id }}">{{ $cance->nombre }}</option>




                @endforeach
                </select>
                 </div>

              <div class="col a12  min-padding center-align">
                Seleccioné los insumos que necesita el niño:<br/>
                    </div>
                <div class="col a12  min-padding center-align">
                    <select name="insumos[]" multiple><option >tipo de insumo</option>
                @foreach ($insumos as $insumo)

                <option value="{{ $insumo->id }}">{{ $insumo->nombre }}</option>


                @endforeach
                  </select>
                 </div>

                <div class="col a12  min-padding center-align">
                Seleccioné los medicamentos que necesita el niño:<br/>
                    </div>
                <div class="col a12  min-padding center-align">
                  <select name="medicamentos[]" multiple> <option >tipo de medicamento</option>
                @foreach ($medicamentos as $medicamento)
  <option value="{{ $medicamento->id }}">{{ $medicamento->nombre }}</option>


                @endforeach
                  </select>
                 </div>


            <div class="col a12 min-padding">
                <input tabindex="8" class="cursor" type="submit" value="save"/>
            </div>

            </div>


          </div>
    </form>
        </div>
    </div>
</div>
