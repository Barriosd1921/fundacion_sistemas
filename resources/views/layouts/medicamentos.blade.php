<div class="level-padding">
    <div class="container">
        <div class="row">
            <div class="col a12">
                <a tabindex="1" href="{{url('/lista')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large red white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            volver
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="container min-padding">
    <div class="row">
        <div class="col a12">
            <form method="POST" action="{{url('/medicamento')}}">
                {{ csrf_field() }}
        <div class="black-text" style="width:100%; max-width:800px; margin:0 auto;">
            
            
            <div class="row">
            
            <div class="col a12 level-padding title-text blue-text">
                seleccione el medicamento
            </div>
            
            
            <div class="col a12 min-padding">
                <select name="id_medicamento"> <!--Supplement an id here instead of using 'name'-->
                    @foreach ($medicamentos as $medicamento)
                    <option value="{{ $medicamento->id }}">{{ $medicamento->nombre }}</option> 
                    @endforeach 
                </select>
            </div>
                
                
                
              <div class="col a12 min-padding">
                <input tabindex="8" class="cursor" type="submit" value="search"/>
            </div>  
                
           
           
            
            </div>
            
            
          </div>            
    </form>
        </div>
    </div>
</div>