









<div class="min-padding">
    <div class="container">
        <div class="row">
            @if(Auth::guest())

            <div class="col a4 t6 level-padding">
                <a tabindex="1" href="{{url('registro')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large white-text center-align z-depth-2" style="background-color:rgb(254,172,249);">
                        <div class="btn-large-container center-align">
                            registraté
                        </div>
                    </div>
                </a>
            </div>
            <div class="col a4 t6 level-padding">
                <a tabindex="2" href="{{url('login')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large blue white-text center-align z-depth-2" style="background-color:rgb(254,172,249);">
                        <div class="btn-large-container center-align">
                            login
                        </div>
                    </div>
                </a>
            </div>
            @else
            <div class="col a8 t12 level-padding">
                <a tabindex="1" href="{{url('data')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large white-text center-align z-depth-2 blue">
                        <div class="btn-large-container center-align">
                            tu cuenta
                        </div>
                    </div>
                </a>
            </div>
            @endif
            <div class="col a4 t12 level-padding">
                <a tabindex="3" href="{{url('lista')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large blue white-text center-align z-depth-2" style="background-color:rgb(254,172,249);">
                        <div class="btn-large-container center-align">
                            ver niños
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="grey-lighten level-padding">
    <div class="level-padding">
    <b class="mintitle-text grey-text">fundación</b><br/>
    <b style="color:rgb(251,72,240); font-size:2.5rem;">ayudando</b><br/>
    <b style="color:rgb(251,72,240); font-size:2.5rem;">manos vacìas</b>
</div>
    <div class="col a12 level-padding">
                <a tabindex="3" href="{{url('atencion')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large animated infinite pulse red white-text center-align z-depth-2">
                        <div class="btn-large-container center-align">
                            atencion al cliente
                        </div>
                    </div>
                </a>
            </div>
    <div class="container grey-text">
        <div class="subtitle-text black-text center-align">
                    amor incondicional
                </div>
                <p>
                    La fundación sin animos de lucro, ayudando manos vacìas, te proporciona información 
                    acerca de los niños con cancer en todo el país, has click en el boton ver niños, 
                    para acceder a todos los datos, tu ayuda es inmensa para nosotros.
                </p>
    </div>
</div>
<div class="overflowHidden galleryLocalGhp">
    <div class="container row">
        <div class="col a12">
            <img alt="niño feliz" class="center-image" style="width:100%; max-width:700px;" src=" {{ asset('assets/image.png') }} "/>
        </div>
    </div>
</div>
