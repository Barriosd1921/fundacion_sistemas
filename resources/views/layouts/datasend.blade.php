<div class="level-padding">
    <div class="container">
        <div class="row">
            <div class="col a4 t12 level-padding">
                <a tabindex="1" href="{{url('/')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large blue white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            home
                        </div>
                    </div>
                </a>
            </div>
            <div class="col a4 t12 level-padding">
                <a tabindex="2" href="{{url('/data')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large blue white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            volver
                        </div>
                    </div>
                </a>
            </div>
            <div class="col a4 t12 level-padding">
                <a tabindex="3" href="{{url('exit')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large red white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            exit
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>




<div class="black-text subtitle-text min-padding">
    NIÑO REGISTRADO EXITOSAMENTE
</div>
