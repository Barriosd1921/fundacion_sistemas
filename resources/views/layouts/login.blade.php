<div class="level-padding">
    <div class="container">
        <div class="row">
            <div class="col a12">
                <a tabindex="1" href="{{url('/')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large blue white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            home
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="container severius-padding">
    <div class="row">
        <div class="col a12">
            <form method="POST" action="{{url('/login')}}">
                {{ csrf_field() }}
        <div class="black-text z-depth-2" style="background-color:rgba(254,172,249,.5); border-radius:25px; width:100%; max-width:300px; margin:0 auto;">
            <div class="level-padding">
    <b class="mintitle-text black-text">fundación</b><br/>
    <b style="color:rgb(251,72,240); font-size:2.5rem;">ayudando</b><br/>
    <b style="color:rgb(251,72,240); font-size:2.5rem;">manos vacìas</b>
</div>
            <div class="min-padding">
                <input tabindex="2" type="email" placeholder="email" name="email" required />
            </div>
            <div class="min-padding">
                <input tabindex="3" type="password" placeholder="password" name="password" required/>
            </div>
            <div class="min-padding">
                <input tabindex="4" class="cursor" type="submit" value="login"/>
            </div>
        </div>    
    </form>
        </div>
    </div>
</div>