<div class="level-padding">
    <div class="container">
        <div class="row">
            <div class="col a6 t12">
                <a tabindex="1" href="{{url('/')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large blue white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            home
                        </div>
                    </div>
                </a>
            </div>
            <div class="col a6 t12">
                <a tabindex="3" href="{{url('exit')}}" style="color:rgb(255,255,255);">
                    <div class="btn btn-large red white-text center-align z-depth-2">
                        <div class="animated infinite pulse btn-large-container center-align">
                            exit
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>



@if(isset($type))
  @if($type == 0)
<div class="black-text subtitle-text min-padding">
    CAMBIO DE CONTRASEÑA EXITOSO
</div>
@endif


@if($type == 1)
<div class="black-text subtitle-text min-padding">
  MEDICAMENTO INGRESADO EXITOSAMENTE
</div>
@endif


@if($type == 2)
<div class="black-text subtitle-text min-padding">
  INSUMO INGRESADO EXITOSAMENTE
</div>
@endif

@if($type == 3)
<div class="black-text subtitle-text min-padding">
  TIPO DE CANCER INGRESADO EXITOSAMENTE
</div>
@endif

@endif



<div class="container level-padding">
    <div class="row">
        <div class="col a12">
            <form method="POST" action="{{url('/resetpassword')}}">
                {{ csrf_field() }}
        <div class="black-text row">
            <div class="col a12 level-padding title-text blue-text">
                Actualizar contraseña
            </div>
            <div class="min-padding col a6 t12">
                <input tabindex="2" type="text" placeholder="email" name="email" required />
            </div>
            <div class="min-padding col a6 t12">
                <input tabindex="3" type="password" placeholder="password" name="password" required/>
            </div>
            <div class="level-padding col a12">
                <input tabindex="8" class="cursor" type="submit" value="reset"/>
            </div>
        </div>
    </form>
        </div>
        <div class="col a6 t12">
            <form method="POST" action="{{url('/create/1')}}">
                {{ csrf_field() }}
        <div class="black-text row">
            <div class="col a12 level-padding title-text blue-text">
                crear nuevo medicamento
            </div>
            <div class="min-padding col a12">
                <input tabindex="2" type="text" placeholder="nombre" name="nombre" required />
            </div>
            <div class="min-padding col a12">
                <input tabindex="2" type="text" placeholder="descripcion" name="descripcion" required />
            </div>
            <div class="level-padding col a12">
                <input tabindex="8" class="cursor" type="submit" value="create"/>
            </div>
        </div>
    </form>
        </div>
        <div class="col a6 t12">
            <form method="POST" action="{{url('/create/2')}}">
                {{ csrf_field() }}
        <div class="black-text row">
            <div class="col a12 level-padding title-text blue-text">
                crear nuevo insumo
            </div>
            <div class="min-padding col a12">
                <input tabindex="2" type="text" placeholder="nombre" name="nombre" required />
            </div>
            <div class="min-padding col a12">
                <input tabindex="2" type="text" placeholder="descripcion" name="descripcion" required />
            </div>
            <div class="level-padding col a12">
                <input tabindex="8" class="cursor" type="submit" value="create"/>
            </div>
        </div>
    </form>
        </div>
        <div class="col a12">
            <form method="POST" action="{{url('/create/3')}}">
                {{ csrf_field() }}
        <div class="black-text row">
            <div class="col a12 level-padding title-text blue-text">
                insertar un nuevo tipo de cancer
            </div>
            <div class="min-padding col a12">
                <input tabindex="2" type="text" placeholder="nombre" name="nombre" required />
            </div>
            <div class="min-padding col a12">
                <input tabindex="2" type="text" placeholder="descripcion" name="descripcion" required />
            </div>
            <div class="level-padding col a12">
                <input tabindex="8" class="cursor" type="submit" value="create"/>
            </div>
        </div>
    </form>
        </div>
    </div>
</div>
