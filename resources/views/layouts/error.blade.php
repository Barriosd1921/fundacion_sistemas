@if( isset($type) )
   @if($type == 'contactanos')
      <div class="red level-padding white-text">
          <div class="level-padding">
              <div class="container row">
                  <div class="col a12 center-align">
                      <img alt="data no encontrada" src="{{asset('assets/notfounddata.png')}}" />
                  </div>
              </div>
          </div>
          <div class="container">
              Lo sentimos no estás registrado en la base de datos,
              ponte en contacto conmigo mediante las diferentes redes sociales 
              que existen, o bien puedes dejarme un mensaje
          </div>
      </div>
   @endif
@endif